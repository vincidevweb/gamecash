import { PanierService } from './../panier.service';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Client} from '../client';
import { Jeux } from '../jeux';
import { FormGroup, FormBuilder, FormArray, Validators, FormControl, ReactiveFormsModule} from '@angular/forms';
import { CommandeService } from '../commande.service';
import { Commande } from '../commande';
import { Panier } from '../panier';
import { DatePipe, formatDate } from '@angular/common';

@Component({
  selector: 'app-formulaire',
  templateUrl: './formulaire.component.html',
  styleUrls: ['./formulaire.component.css']
})
export class FormulaireComponent implements OnInit {

  allGames: Jeux[] ;
  modelform: FormGroup ;
  client: Client ;
  commande = new Commande();
  panier: Panier;
  format = 'dd/MM/yyyy hh-mm-ss';
  myDate = new Date();
  formattedDate = this.myDate.toLocaleDateString();
  commandeEnvoyeOk = false;
  commandeOnRoute = true ;
 constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private commandeService: CommandeService,
    private panierService: PanierService
  ) { }

  ngOnInit() {
    this.panier = this.panierService.getPanierClient();
    this.modelform = this.formBuilder.group({
      nom: ['', [Validators.required]],
      prenom: ['', [Validators.required]],
      email: ['', [Validators.required, Validators.email ]],
      tel: ['', [Validators.required]],
      nRue: ['', [Validators.required]],
      adresse : ['', [Validators.required]],
      codePostal: ['', [Validators.required]],
      ville: ['', [Validators.required]]

    }) ;
   }

  get nom() {
    return this.modelform.get('nom');
  }
  get prenom() {
    return this.modelform.get('prenom');
  }
  get email() {
    return this.modelform.get('email');
  }
  get tel() {
    return this.modelform.get('tel');
  }
  get nRue() {
    return this.modelform.get('nRue');
  }
  get adresse() {
    return this.modelform.get('adresse');
  }
  get codePostal() {
    return this.modelform.get('codePostal');
  }
  get ville() {
    return this.modelform.get('ville');
  }
  validationForm() {
    if (this.modelform.valid) {
      this.client = this.modelform.value;
      this.commande.client = this.client;
      this.commande.panier = this.panier;
      this.commande.dateCommande = this.formattedDate;
      this.commandeService.addCommandeDb(this.commande).subscribe(
        result => {
          if (result) {
            let newCommande: any = new Commande();            
            newCommande = result ;
            this.commandeService.setCommande(newCommande);
            this.commandeEnvoyeOk = true ;
            this.router.navigateByUrl('/vente');
          }}) ;
    }
  }


}
