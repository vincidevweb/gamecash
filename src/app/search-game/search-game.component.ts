import { Component, OnInit, Input } from '@angular/core';
import { FormBuilder, FormGroup , FormsModule, Validators} from '@angular/forms';
import { Jeux } from '../jeux';
import { GamesService } from '../games.service';
import { PanierService } from '../panier.service';

@Component({
  selector: 'app-search-game',
  templateUrl: './search-game.component.html',
  styleUrls: ['./search-game.component.css']
})
export class SearchGameComponent implements OnInit {

  model: FormGroup ;
  allGames: Jeux[] ;
  searchQuery: string ;

  constructor(private formBuilder: FormBuilder, private gamesService: GamesService, private panierService: PanierService ) { }

  ngOnInit() {
    this.model = this.formBuilder.group({
       search: '' ,
    });
  }
  ajouterAuPanier(jeu) {
      this.panierService.addJeuxToPanier(jeu);
  }
  rafraichirListe(query) {
    query = query.trim();
    if (query !== '' && query.length > 2) {
    this.gamesService.searchGameByTitre(query).subscribe(
      result => {
        if (result.length > 0) { this.allGames = result; } else {
          this.gamesService.searchGameByCodeBarre(query).subscribe (
          result2 => { if (result2.length > 0) { this.allGames = result2; } else { this.allGames = [] ; } }) ;
        }
       }
         );
    } else {
      this.allGames = [];
    }
  }
}
