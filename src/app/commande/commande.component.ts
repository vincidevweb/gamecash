import { Component, OnInit } from '@angular/core';
import { CommandeService } from '../commande.service';
import { Commande } from '../commande';


@Component({
  selector: 'app-commande',
  templateUrl: './commande.component.html',
  styleUrls: ['./commande.component.css']
})
export class CommandeComponent implements OnInit {

  commande = new Commande();

  constructor(private commandeService: CommandeService) {
  }
  ngOnInit() {
    this.commande = this.commandeService.getCurrentCommande();
  }

}
