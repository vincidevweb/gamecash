import { Component, OnInit, Input } from '@angular/core';
import { Jeux } from '../jeux';
import { Router, ActivatedRoute } from '@angular/router';
import { PanierService } from '../panier.service';

@Component({
  selector: 'app-panier',
  templateUrl: './panier.component.html',
  styleUrls: ['./panier.component.css']
})
export class PanierComponent implements OnInit {

  monPanier: Jeux[];
  totalJeuxAvendre = 0;
  totalPrixAchat = 0;
  routeMonPanier: boolean ;

  constructor(
    private panierService: PanierService,
    private router: Router,
    private route: ActivatedRoute
    ) {

   }

  ngOnInit() {
    this.routeMonPanier = this.router.url.startsWith('/panier') ;
    this.monPanier = this.panierService.getPanier();
    this.panierService.totalJeux.subscribe(nouveauTotal => this.totalJeuxAvendre = nouveauTotal);
    this.panierService.totalPrixAchat.subscribe(nouveauTotal => this.totalPrixAchat = nouveauTotal);
  }
  enleverDuPanier(jeux: Jeux) {
    this.panierService.deleteJeuxToPanier(jeux);
  }
  confirmationCommande() {
  }

}
