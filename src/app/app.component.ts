import { Component } from '@angular/core';
import { PanierService } from './panier.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'gamecash';
  totalJeuxAvendre: any;
  totalPrixAchat: any;

  constructor(private panierService: PanierService) {
    this.panierService.totalJeux.subscribe(nouveauTotal => this.totalJeuxAvendre = nouveauTotal);
    this.panierService.totalPrixAchat.subscribe(nouveauTotal => this.totalPrixAchat = nouveauTotal);

  }
}
