import { Injectable } from '@angular/core';
import { Jeux } from './jeux';
import { BehaviorSubject } from 'rxjs';
import { Panier } from './panier';

@Injectable({
  providedIn: 'root'
})
export class PanierService {

  panier: Jeux[] = [];
  panierClient = new Panier();
  totalJeux = new BehaviorSubject(0);
  currentTotalJeux = this.totalJeux.asObservable();

  totalPrixAchat = new BehaviorSubject(0);
  currentTotalPrixAchat = this.totalPrixAchat.asObservable();

  constructor() { }

  addJeuxToPanier(jeux: Jeux) {
    // correction bug string prix Achat
    if (typeof jeux.prixAchat === 'string') {
      jeux.prixAchat = jeux.prixAchat.replace(',', '.').trim();
    }
    this.panier.push(jeux);

    this.panierClient.jeux = this.panier;
    this.panierClient.nombreJeux = this.panier.length ;

    this.setTotalJeux(this.panier.length);
    let totalprix = 0 ;
    this.panier.map(result => {
        totalprix = totalprix + parseFloat(result.prixAchat);
        return totalprix;
    });
    this.setTotalPrixAchat(totalprix.toFixed(2));
    this.panierClient.prixAchatTotal = totalprix.toFixed(2) ;
  }
  deleteJeuxToPanier(jeux: Jeux) {

    this.panier.splice(this.panier.findIndex(v => v.id === jeux.id), 1);

    this.panierClient.jeux = this.panier;
    this.panierClient.nombreJeux = this.panier.length ;
    this.setTotalJeux(this.panier.length);
    let totalprix = 0 ;
    this.panier.map(result => {
      totalprix = totalprix + parseFloat(result.prixAchat);
      return totalprix;
  });
    this.setTotalPrixAchat(totalprix.toFixed(2));
    this.panierClient.prixAchatTotal = totalprix.toFixed(2) ;
  }
  getPanier(): Jeux[] {
    return this.panier ;
  }
  getPanierClient(): Panier {
    console.log(this.panierClient);
    return this.panierClient;
  }
  getTotalJeux() {
    return this.totalJeux ;
  }
  setTotalJeux(nombre) {
    this.totalJeux.next(nombre);
  }
  setTotalPrixAchat(price) {
    this.totalPrixAchat.next(price);
  }
  getTotalprixAchat() {
    return this.totalPrixAchat;
  }
  CalculTotalPrixachat(): any {
    return 0;
  }
}
