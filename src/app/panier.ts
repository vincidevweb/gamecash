import { Jeux } from './jeux';

export class Panier {
    id? ;
    jeux: Jeux[];
    nombreJeux: number;
    prixAchatTotal: string;
}
