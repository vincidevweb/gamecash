import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { PanierComponent } from './panier/panier.component';
import { CommandeComponent } from './commande/commande.component';
import { FormulaireComponent } from './formulaire/formulaire.component';


const routes: Routes = [
  { path: 'home', component: HomeComponent },
  { path: 'panier', component: PanierComponent },
  { path: 'vente', component: CommandeComponent },
  { path: 'formulaire', component: FormulaireComponent},
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  { path: '**', component: PageNotFoundComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
