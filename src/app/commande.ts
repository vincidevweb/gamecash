import { Panier } from './panier';
import { Client } from './client';

export class Commande {
        panier: Panier ;
        client: Client ;
        dateCommande: string ;
        id?: number ;
}
