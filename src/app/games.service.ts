import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Jeux } from './jeux';
import { environment } from 'src/environments/environment';
@Injectable({
  providedIn: 'root'
})
export class GamesService {

  constructor(private http: HttpClient) { }

  public getGames(): Observable<Jeux[]> {
    return this.http.get<Jeux[]>(environment.backUrl + '/games');
  }
  public getGame(id: number): Observable<Jeux> {
    return this.http.get<Jeux>(environment.backUrl + '/games/' + id);
  }

  public addGame(jeux: Jeux) {
    return this.http.post(environment.backUrl + '/games', jeux);
  }

  public delGame(id: number) {
    return this.http.delete(environment.backUrl + '/games/' + id);
  }
  public editGame(jeux: Jeux) {
    return this.http.put(environment.backUrl + '/games/' + jeux.id, jeux);
  }
  // public searchGame(titre: string): Observable<Jeux[]>{
  //   return this.http.get<Jeux[]>(environment.backUrl + '/games?titre=' + titre);
  // }
  public searchGameByTitre(query: any): Observable<Jeux[]> {
    const result: any =  this.http.get<Jeux[]>(environment.backUrl + '/games?titre_like=' + query + '&_limit=5') ;
    return result ;
   }
   public searchGameByCodeBarre(query: any): Observable<Jeux[]> {
    const result: any =  this.http.get<Jeux[]>(environment.backUrl + '/games?codeBarre_like=' + query + '&_limit=5') ;
    return result ;
   }
}


