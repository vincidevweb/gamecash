import { Injectable } from '@angular/core';
import { Commande } from './commande';
import { HttpClient } from '@angular/common/http';
import { Observable, BehaviorSubject } from 'rxjs';
import { environment } from 'src/environments/environment';



@Injectable({
  providedIn: 'root'
})
export class CommandeService {

  commande: Commande ;

  constructor(private http: HttpClient) { }
  public addCommandeDb(commande: Commande) {
    return this.http.post(environment.backUrl + '/commandes', commande);
  }
  getCurrentCommande(): Commande {
    return this.commande;
  }
  setCommande(result: Commande) {
    this.commande = result ;
  }
}
