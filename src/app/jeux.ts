export class Jeux {
    id: number;
    titre: string;
    imageUrl: string;
    prixAchat: string;
    codeBarre: number;
    console: string;

    description?: string;
    prixRevente?: number;
    editeur?: string;
    anneeDeSortie?: number;
    constructor(newTitre: string, newImage: string) {
        this.titre = newTitre;
        this.imageUrl = newImage;
    }
}
